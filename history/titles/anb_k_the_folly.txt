d_nathalaire = {
	909.6.30 = {
		holder = orstengard_0002
	}
	943.12.28 = {
		holder = orstengard_0004
	}
	961.10.8 = {
		holder = orstengard_0005
	}
	994.3.6 = {
		holder = orstengard_0006
	}
	1007.9.2 = {
		holder = orstengard_0007
	}
}

c_nathalaire = {
	1000.1.1 = { change_development_level = 13 }
	909.6.30 = {
		holder = orstengard_0002
	}
	943.12.28 = {
		holder = orstengard_0004
	}
	961.10.8 = {
		holder = orstengard_0005
	}
	994.3.6 = {
		holder = orstengard_0006
	}
	1007.9.2 = {
		holder = orstengard_0007
	}
}

d_daravan_folly = {
	1014.1.1 = {
		liege = e_bulwar
		holder = sun_elvish0005	#Denarion
	}
}

c_lorest_watch = {
	1000.1.1 = { change_development_level = 3 }
	1014.1.1 = {
		liege = e_bulwar
		holder = sun_elvish0005	#Denarion
	}
}

c_corveld_coast = {
	1000.1.1 = { change_development_level = 3 }
}

c_448_test = {
	1000.1.1 = { change_development_level = 3 }
}

c_dreadmire = {
	1000.1.1 = { change_development_level = 3 }
}

c_nathfort = {
	1000.1.1 = { change_development_level = 3 }
}

c_xhazobain_end = {
	1000.1.1 = { change_development_level = 3 }
}