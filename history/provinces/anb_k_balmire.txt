#k_balmire
##d_bal_mire
###c_bal_mire
229 = {		#Bal Mire

	# Misc
	culture = balmirish
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_mire_01
		special_building = castanorian_citadel_bal_mire_01
	}
}
2454 = {

    # Misc
    holding = church_holding

    # History

}
2455 = {

    # Misc
    holding = city_holding

    # History

}
2456 = {

    # Misc
    holding = none

    # History

}
2457 = {

    # Misc
    holding = none

    # History

}

###c_cravens_walk
231 = {		#Craven's Walk

	# Misc
	culture = balmirish
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2447 = {

    # Misc
    holding = church_holding

    # History

}
2448 = {

    # Misc
    holding = none

    # History

}
2449 = {

    # Misc
    holding = none

    # History

}

###c_rottenstep
230 = {		#Rottenstep

	# Misc
	culture = balmirish
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2450 = {

    # Misc
    holding = city_holding

    # History

}
2451 = {

    # Misc
    holding = none

    # History

}
2452 = {

    # Misc
    holding = none

    # History

}
2453 = {

    # Misc
    holding = none

    # History

}

###c_venomfen
232 = {		#Venomfen

	# Misc
	culture = balmirish
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2458 = {

    # Misc
    holding = none

    # History

}
2459 = {

    # Misc
    holding = church_holding

    # History

}
2460 = {

    # Misc
    holding = none

    # History

}

##d_middle_alen
###c_entalenham
2490 = {	#Entalenham

	# Misc
	culture = white_reachman
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
725 = {

    # Misc
    holding = church_holding

    # History

}
2489 = {

    # Misc
    holding = none

    # History

}

###c_mirelook
710 = {		#Mirelook

	# Misc
	culture = white_reachman
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2487 = {

    # Misc
    holding = none

    # History

}

###c_headmans_wood
726 = { #Headman's Wood

	# Misc
	culture = white_reachman
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2488 = {

    # Misc
    holding = none

    # History

}

##d_cannwood
###c_cannwood
728 = {		#Cannwood

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2474 = {

    # Misc
    holding = church_holding

    # History

}
2475 = {

    # Misc
    holding = none

    # History

}

###c_forksgrove
727 = {		#Forksgrove

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2469 = {

    # Misc
    holding = church_holding

    # History

}
2470 = {

    # Misc
    holding = none

    # History

}

###c_cannwic
750 = {		#Cannwic

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2471 = {

    # Misc
    holding = none

    # History

}
2472 = {

    # Misc
    holding = none

    # History

}
2473 = {

    # Misc
    holding = city_holding

    # History

}

###c_westwall_wood
758 = {

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2483 = {

    # Misc
    holding = city_holding

    # History

}
2484 = {

    # Misc
    holding = none

    # History

}

###c_westwatch
2481 = {		#Westwall Wood

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
753 = {

    # Misc
    holding = city_holding

    # History

}
2482 = {

    # Misc
    holding = none

    # History

}

###c_esckerfort
751 = {	

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2485 = {

    # Misc
    holding = church_holding

    # History

}
2486 = {

    # Misc
    holding = none

    # History

}

##d_agradalen
###c_agradord
2477 = {	#Esckerfort

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2478 = {

    # Misc
    holding = none

    # History

}
2479 = {

    # Misc
    holding = none

    # History

}
2480 = {		#Westwatch

	# Misc
	holding = city_holding

	# History
}

###c_ridgehyl
2468 = {

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2476 = {

    # Misc
    holding = none

    # History

}

###c_athfork
729 = {		#Athfork

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2491 = {

    # Misc
    holding = church_holding

    # History

}
2492 = {

    # Misc
    holding = none

    # History

}
2493 = {

    # Misc
    holding = none

    # History

}
2494 = {

    # Misc
    holding = city_holding

    # History

}

###c_uelced
749 = {		#Uelced

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2495 = {

    # Misc
    holding = city_holding

    # History

}
2496 = {

    # Misc
    holding = none

    # History

}