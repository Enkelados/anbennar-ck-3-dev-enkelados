﻿
# Empires

e_alenor = {
	item = {
		trigger = {
			holder.culture = culture:gawedi
		}
		coat_of_arms = e_alenor_gawed	
	}
	item = {
		trigger = {
			NOT = { holder.culture = culture:gawedi }
		}
		coat_of_arms = e_alenor	
	}
}

e_bulwar = {
	item = {
		trigger = {
			holder.culture = culture:sun_elvish
		}
		coat_of_arms = e_phoenix_empire	
	}
	item = {
		trigger = {
			NOT = { holder.culture = culture:sun_elvish }
		}
		coat_of_arms = e_bulwar	
	}
}

e_castanor = {
	item = {
		trigger = {
			OR = {
				holder.culture = { has_cultural_pillar = heritage_gerudian }
				holder.culture = culture:black_castanorian
				holder.dynasty = dynasty:dynasty_ebonfrost
			}
		}
		coat_of_arms = e_black_castanor	
	}
	item = {
		trigger = {
			NOT = {
				OR = {
					holder.culture = { has_cultural_pillar = heritage_gerudian }
					holder.culture = culture:black_castanorian
					holder.dynasty = dynasty:dynasty_ebonfrost
				}
			}
		}
		coat_of_arms = e_castanor	
	}
}

# e_dragon_coast = {
	# item = {
		# trigger = {
			# holder.culture = { has_cultural_pillar = heritage_gnomish }
		# }
		# coat_of_arms = e_gnomish_hierarchy	
	# }
	# item = {
		# trigger = {
			# NOT = { holder.culture = { has_cultural_pillar = heritage_gnomish } }
		# }
		# coat_of_arms = e_dragon_coast	
	# }
# }

# Kingdoms

k_dameria = {
	item = {
		trigger = {
			holder.culture = culture:old_damerian
		}
		coat_of_arms = k_old_dameria	
	}
	item = {
		trigger = {
			NOT = { holder.culture = culture:old_damerian }
		}
		coat_of_arms = k_dameria	
	}
}

# Duchies

d_verne = {
	item = {
		trigger = {
			OR = {
				holder.primary_title = title:k_dameria
				holder = {
					any_liege_or_above = {
						this.primary_title = title:k_dameria
					}
				}
			}
		}
		coat_of_arms = d_verne_dameria
	}
	item = {
		trigger = {
			NOT = {
				OR = {
					holder.primary_title = title:k_dameria
					holder = {
						any_liege_or_above = {
							this.primary_title = title:k_dameria
						}
					}
				}
			}
		}
		coat_of_arms = d_verne
	}
}
