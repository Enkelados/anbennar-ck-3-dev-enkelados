﻿anb_game_start = {
	effect = {
		set_immortal_age_for_scripted_characters = yes
		
		setup_racial_traits_for_all_characters = yes
		racial_lifestyles_setup = yes	#linked to traits above

		every_living_character = {
			limit = {
				has_culture = culture:pearlsedger
			}
			if = {
				limit = {
					NOT = { has_character_flag = pearlsedge_bald_flag }
					OR = {
						trigger_if = {
							limit = { is_female = no }
							is_married = yes
						}
						trigger_if = {
							limit = { is_female = yes }
							martial >= 30
						}
						trigger_else = {
							martial >= 20
						}
						trigger_if = {
							limit = { is_female = yes }
							prowess >= 30
						}
						trigger_else = {
							prowess >= 20
						}
					}
				}
				add_character_flag = {
					flag = pearlsedge_bald_flag
					days = -1
				}
				add_prestige = 10
			}
		}

		title:e_bulwar = { set_coa = e_phoenix_empire }
		
		###Anbennar - Cult of nerat has been reformed into court of nerat.
		faith:cult_of_nerat = { 
			set_variable = { name = has_been_reformed }
		}
		
		mark_faiths_to_found = yes
		
		# Setup bilingualism
		setup_bilingual_traditions = yes
		
		# 1022 Regents/Viziers # Anbennar TODO: make this a scripted effect, so this file doesnt require much editing
		if = { # Anbennar
			limit = { game_start_date = 1022.1.1 }
			# Designate some regents.
			## Phoenix emperor Jaher and Firinar aralzuir
			character:sun_elvish0001 = {
				designate_diarch = character:sun_elvish0036
				# This is a vizierate as well, so start the diarchy manually.
				start_diarchy = vizierate
				# Tell Jaher that he appointed Firinar so he remembers not to dismiss him.
				set_variable = {
					name = my_vizier
					value = character:sun_elvish0036
				}
			}
		}
	}
}

anb_on_game_start_after_lobby = {
	events = {
		anb_harpy_initialization.0001 #Get rid of male "harpies"
	}
	effect = {
		#Anbennar - Setup Racial Purity
		if = {
			limit = { has_game_rule = legitimacy_and_supremacy_on }
			#Add racial attitude to characters
			every_living_character = {
				limit = { NOT = { has_variable = no_purist_trait } } # Don't give it to characters marked as not racist
				roll_racial_purity_trait_for_character = yes
			}
		}
		else = {
			every_living_character = {
				remove_racial_purist_traits = yes
			}
		}

		if = {
			limit = {
				OR = {
					has_game_rule = starting_randomized_races
					has_game_rule = all_randomized_races
				}
			}
			every_living_character = {
				set_random_racial_trait = yes
			}
		}
	}
}